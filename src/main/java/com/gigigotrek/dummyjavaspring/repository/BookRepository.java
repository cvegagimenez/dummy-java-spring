package com.gigigotrek.dummyjavaspring.repository;

import com.gigigotrek.dummyjavaspring.data.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends CrudRepository<Book, Long>{
    List<Book> findByTitle(String title);

    List<Book> findByTitleAndAuthor(String  title, String author);

}
